﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Test
{
    public partial class Form1 : Form
    {
        private Thread myThread;
        private static ManualResetEvent manualResetEvent;

        private bool flagStop = false;
        private bool searchedFiles = false;
        private int seconds = 0;
        private const string Metacharacter = "*";
        private const char PathSeparator = '\\';

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SetDefaultValues();
            SetButtonEnablence(false);
            SetTextBoxEnablence(false);

            manualResetEvent = new ManualResetEvent(true);
            Thread mainThread = new Thread(Search)
            {
                IsBackground = true
            };
            mainThread.Start();
        }

        private void Search()
        {
            manualResetEvent.WaitOne();
            this.Invoke((MethodInvoker)delegate ()
            {
                seconds = 0;
                TimeSpan currentTime = TimeSpan.FromSeconds(seconds);
                label3.Text = $"Время поиска: {currentTime:hh\\:mm\\:ss}";
                timer1.Start();
            });
            searchedFiles = false;

            myThread = Thread.CurrentThread;

            string dirName = textBox2.Text;

            if (Directory.Exists(dirName))
            {
                string template = (textBox1.Text == String.Empty) ? Metacharacter : textBox1.Text;

                TreeNode root = new TreeNode();
                TreeNode node = root;

                this.Invoke((MethodInvoker)delegate ()
                {
                    treeView1.Nodes.Add(root);
                });

                try
                {
                    string[] files = Directory.GetFiles(dirName, template, SearchOption.AllDirectories);

                    for (int iterator = 0; iterator < files.Length; iterator++)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            label6.Text = $"В данный момент обрабатывается {files[iterator]}";
                        });

                        if (textBox3.Text == String.Empty)
                        {
                            FillTree(ref node, ref root, files, iterator);
                        }
                        else
                        {
                            using (StreamReader sr = new StreamReader(files[iterator], Encoding.Default))
                            {
                                string line;
                                while ((line = sr.ReadLine()) != null)
                                {
                                    manualResetEvent.WaitOne();
                                    if (line.ToLower().Contains(textBox3.Text.ToLower()))
                                    {
                                        FillTree(ref node, ref root, files, iterator);
                                        break;
                                    }
                                }
                            }
                        }
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            label5.Text = $"Количество обработанных файлов: {iterator + 1}";
                            label6.Text = String.Empty;
                        });
                    }
                    if (!searchedFiles)
                    {
                        this.Invoke((MethodInvoker)delegate ()
                        {
                            label5.Text = "По заданным критериям искомых файлов не найдено";
                        });
                    }
                }
                catch (ThreadAbortException) { }
                catch (SystemException ex)
                {
                    MessageBox.Show($"Возникло исключение: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                this.Invoke((MethodInvoker)delegate ()
                {
                    label5.Text = "Такой директории не найдено";
                });
            }

            this.Invoke((MethodInvoker)delegate ()
            {
                SetButtonEnablence(true);
                SetTextBoxEnablence(true);

                label4.Text = "Поиск завершен";
            });

            this.Invoke((MethodInvoker)delegate ()
            {
                timer1.Stop();
            });
            seconds = 0;
        }

        private void FillTree(ref TreeNode node, ref TreeNode root, string[] files, int iterator)
        {
            TreeNode nodeBuf = root;
            searchedFiles = true;

            foreach (string pathBits in files[iterator].Split(PathSeparator))
            {
                manualResetEvent.WaitOne();
                this.Invoke((MethodInvoker)delegate ()
                {
                    nodeBuf = AddNode(nodeBuf, pathBits);
                    treeView1.ExpandAll();
                    treeView1.Refresh();
                });
            }
            node = nodeBuf;
        }

        private TreeNode AddNode(TreeNode node, string key)
        {
            if (node.Nodes.ContainsKey(key))
            {
                return node.Nodes[key];
            }
            else
            {
                return node.Nodes.Add(key, key);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!flagStop)
            {
                button2.Text = "Продолжить";
                manualResetEvent.Reset();
                timer1.Stop();
                flagStop = true;
            }
            else
            {
                button2.Text = "Пауза";
                manualResetEvent.Set();
                timer1.Start();
                flagStop = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (flagStop)
            {
                button2.Text = "Пауза";
                flagStop = false;
            }

            timer1.Stop();
            manualResetEvent.Reset();

            SetTextBoxEnablence(true);
            SetButtonEnablence(true);
            SetDefaultValues();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myThread != null)
            {
                myThread.Abort();
            }
            Properties.Settings.Default["searchTemplate"] = textBox1.Text;
            Properties.Settings.Default["searchDirectory"] = textBox2.Text;
            Properties.Settings.Default["searchValue"] = textBox3.Text;
            Properties.Settings.Default.Save();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Invoke((MethodInvoker)delegate ()
            {
                seconds++;
                TimeSpan currentTime = TimeSpan.FromSeconds(seconds);
                label3.Text = $"Время поиска: {currentTime:hh\\:mm\\:ss}";
            });
        }

        private void SetDefaultValues()
        {
            label3.Text = String.Empty; 
            label4.Text = String.Empty;
            label5.Text = String.Empty;
            label6.Text = String.Empty;
            treeView1.Nodes.Clear();
        }

        private void SetTextBoxEnablence(bool enable)
        {
            textBox1.Enabled = enable;
            textBox2.Enabled = enable;
            textBox3.Enabled = enable;
        }

        private void SetButtonEnablence(bool enable)
        {
            button1.Enabled = enable;
            button2.Enabled = !enable;
            button3.Enabled = !enable;
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 aboutBox = new AboutBox1();
            aboutBox.ShowDialog();
        }
    }
}